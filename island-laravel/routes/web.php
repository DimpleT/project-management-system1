<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::any('/home', 'HomeController@index')->name('home');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::middleware(['auth'])->group(function () {

  Route::any('/{user}/boards','BoardController@index')->name('boards');
  Route::get('/user/find', 'UserController@find');
  Route::post('/user/invite-user', 'UserController@inviteUser');

  Route::prefix('board')->group(function () {
      Route::post('/create','BoardController@create')->name('createBoard');
      Route::any('/change-card','BoardController@changeCard');
      Route::post('/add-list','BoardController@addList')->name('addList');
	  Route::post('/rename-list','BoardController@renameList')->name('renameList');
      Route::post('/rename-card','BoardController@renameCard')->name('renameCard');
      Route::post('/add-card','BoardController@addCard')->name('addCard');
      Route::post('/sortable-workflow','BoardController@updateWorkflowOrder');
      Route::post('/sortable-cards','BoardController@updateCardOrder');
      Route::post('/delete/{id}','BoardController@delete')->name('deleteBoard');
      Route::get('/delete-list/{id}','BoardController@deleteList');
      Route::get('/{folder}/{name}','BoardController@view')->name('boardView');
  });

});
