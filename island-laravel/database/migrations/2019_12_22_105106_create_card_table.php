<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	     if (Schema::hasTable('card')) {
	       return;
	     }
        Schema::create('card', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_workflow')->unsigned();
            $table->string('title');
            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_general_ci';

            $table->index('id_workflow');

            $table->foreign('id_workflow')
              ->references('id')->on('workflow')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
