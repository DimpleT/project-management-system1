<div class="card list" data-id="{{ $workflow->id }}">
	<div class="card-header" style="padding: 10px 10px 10px 15px;color: white; position: relative">
        <div class="list-header-target js-editing-target"></div>
        <input type="text" class="label-card-title card-title-normal form-control form-control-sm"  name="title" value="{{ $workflow->name }}">
      <div class="workflow-menu" style="float:right;position:relative;">
        <div class="btn-list-menu">
          <img src="{{ asset('assets/img/more.png') }}">
        </div>
          <ul class="dropdown-menu list-menu" role="menu" aria-labelledby="dropdownMenu" x-placement="bottom-start">
              <a class="dropdown-item" href="#">Action</a>
              <a class="dropdown-item" href="#">Another action</a>
              <a class="dropdown-item" onclick="return confirm('Are you sure?')" href="{{ url('/board/delete-list', ['id' => $workflow->id]) }}">Delete</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Something else here</a>
          </ul>
      </div>
	</div>
	<div class="card-body">
        <div class="cards workflow-{{ $workflow->id }}" data-workflowId="{{ $workflow->id }}">
            @foreach ($workflow->cards()->orderBy('order', 'asc')->get() as $card)
                @include('board/_card', ['card' =>  $card])
            @endforeach
        </div>
        <div class="add-card" style="display:none">
          {{ Form::open(['action' => 'BoardController@addCard', 'id' => 'add-card-form-' . $workflow->id]) }}
              {{ Form::textarea("title", '', ['class' => 'form-control text-card', 'rows' => '3' ,'data-id' => $workflow->id, 'placeholder' => "Ввести заголовок для этой карточки"]) }}
              {{ Form::hidden('listId', $workflow->id) }}
              {{ Form::button("Create", ['class' => 'btn btn-success submit-add-card', 'style' => 'width:100px;margin-top:5px;inline-block', 'data-id' => $workflow->id]) }}
              {{ Form::button("X", ['class' => 'close-add-card-form', 'data-id' => $workflow->id]) }}
          {{ Form::close() }}
        </div>
        <div class="btn-add-card" data-id="{{ $workflow->id }}">+ Add card</div>
	</div>
</div>

