<div class="window" style="display: block;height:200px;padding: 20px 35px 20px 20px ;">
    <div class="window-wrapper js-tab-parent" data-elevation="1">
        <a class="icon-md icon-close dialog-close-button js-close-window" href="#"></a>
        {{ Form::open(['action' => 'BoardController@changeCard', 'id' => 'change-card-form']) }}
        {{ Form::text("title", $card->title, ['data-id' => $card->id, 'class' => 'form-control card-title', 'placeholder' => "Ввести заголовок списка", 'autocomplete' => 'off']) }}
{{--        {{ Form::button('Save', ['id' => 'change-card', 'type' => 'submit', 'class' => "btn btn-success"]) }}--}}
        {{ Form::close() }}
    </div>
</div>

<script>
    $(".card-title").on('blur', function(e){
        var title = $(this);
        var id = $(this).data('id');
        $.ajax({
            type: "POST",
            url: renameCardUrl,
            data: {
                id: id,
                title: title.val(),
                _token: $('meta[name="csrf-token"]').attr('content')
            },
            success: function(response)
            {
                $('a[data-id=' + id + '] > span').html(title.val());
            }
        });
    });
</script>

