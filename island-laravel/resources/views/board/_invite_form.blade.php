<div id="invite-users" class="pop-over board-member-add-multipl" data-elevation="1" style="left: 0; top: 40px;z-index: 1051;">
	<div class="no-back">
		<div class="pop-over-header js-pop-over-header">
			<span class="pop-over-header-title">Invite To Board</span>
			<a href="#" id="btn-close-invite" class="pop-over-header-close-btn icon-sm icon-close"></a>
		</div>
		<div>
			<div class="pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent" style="max-height: 829px;">
				<div>
					<div>
						<div class="invite-board-member-autocomplete js-autocomplete-root">
							<div class="multi-select-autocomplete-container">
								{{ Form::open(['action' => 'UserController@inviteUser', 'id' => 'invite-user-form']) }}
                                {{ Form::hidden('id_board', $boardId) }}
									<div class="autocomplete-input-container is-empty">
										<div class="autocomplete-selected">
											{{ Form::select('user_list[]', [], [], [
											'id' => "user_list",
											'multiple' => 'multiple',
											'placeholder' => "Email address or name",
											'class' => "form-control",
											'style' => "width: 100%"]) }}
										</div>
									</div>
									{{ Form::button('Send Invitation', ['id' => 'send_invitation', 'type' => 'submit', 'class' => "autocomplete-btn primary"]) }}
								{{ Form::close() }}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@push('scripts')
	<script>
		$('#user_list').select2({
			placeholder: "Choose users...",
			minimumInputLength: 2,
			ajax: {
				url: userFindUrl,
				dataType: 'json',
				type: 'get',
				delay: 250,
				data: function (params) {
					return {
						q: $.trim(params.term)
					};
				},
				processResults: function(data, params) {
					var payload = {
						results: data
					};
					return payload;
				},
				cache: true
			},
			templateResult: function(result) {
				console.log('templateResult', result);
				return result.name || result.text;
			},
			templateSelection: function(selection) {
				console.log('templateSelection', selection);
				return selection.name || selection.email;
			}
		});
	</script>
@endpush
