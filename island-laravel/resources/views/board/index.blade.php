@extends('layouts.main')

@section('title', 'Boards')

@push('styles')
    <style>
        .main-header {
           z-index: 1040!important;
        }
    </style>
@endpush

@section('content')
    <div class="content">
        <div class="container">
            <div class="row justify-content-left">
                <div class="col-md-10">
                    <div class="title-boards-group">Personal boards</div>
                    <div class="row">
                        @foreach ($singleBoards as $board)
                            @include('board/_board', ['board' =>  $board])
                        @endforeach
                        <div class="col-md-3">
                            <div id="create-board" class="create-board btn btn-primary btn-lg">
                                <span>Create new board</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="title-boards-group">Team boards</div>
                    <div class="row">
                        @foreach ($teamBoards as $board)
                            @include('board/_board', ['board' =>  $board])
                        @endforeach
                        <div class="col-md-3">
                            <div id="create-board" class="create-board btn btn-primary btn-lg">
                                <span>Create new board</span>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Create new board -->
    <div class="modal fade" id="modalNewBoard" tabindex="-1" role="dialog" aria-labelledby="modalUpdatePro" aria-hidden="true">
        <div class="modal-dialog" style="" role="document">
            <div class="modal-content">
                {{ Form::open(['action' => 'BoardController@create', 'id' => 'create-board-form']) }}
                <div class="modal-header bg-primary">
                    <h6 class="modal-title">Create new board</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center" style="height: 130px;">
                    {{ Form::text("boardName", '', ['class' => 'form-control input-full', 'placeholder' => 'Name', 'required' => '']) }}
                    {{ Form::label('type' , 'Type:', ['class' => 'col-form-label', 'style' => 'float:left;margin-right: 10px;']) }}
                    {{ Form::select('type', [\App\Models\Board::$TYPE_SINGLE => 'Single', \App\Models\Board::$TYPE_TEAM => 'Team'], null, ['class' => 'form-control form-control', 'style' => 'width:50%;float:left']) }}
                    {{ Form::label('boardBG' , 'Background:', ['class' => 'col-form-label ', 'style' => 'float:left;margin-right: 10px;']) }}
                    <div class="pop-over is-shown" style="left: 512px; top: 0px;" data-elevation="2">
                        <div class="no-back">
                            <div class="pop-over-header js-pop-over-header">
                                <span class="pop-over-header-title">Фон доски</span>
                                <a href="#" class="pop-over-header-close-btn icon-sm icon-close"></a>
                            </div>
                            <div>
                                <div class="pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent" style="max-height: 843px;">
                                    <div tabindex="0" class="background-chooser">
                                        <section>
                                            <header class="background-chooser-header">
                                                <h1 class="background-chooser-heading">
                                                    <span>Фотографии</span>
                                                </h1>
                                                <a href="#" class="quiet-button">
                                                    <span>Подробнее</span>
                                                </a>
                                            </header>
                                            <ul class="background-grid" id="background-photo">
                                                <?php foreach ($bgImages as $bgImage): ?>
                                                    <li class="background-grid-item">
                                                        <div class="background-grid-trigger is-photo" role="button" data-full="<?=$bgImage->urls->full?>" style="background-image: url(<?=$bgImage->urls->small?>);">
                                                            <span class="icon-sm icon-check"></span>
                                                            <div class="photo-attribution-component">
                                                                <a href="<?= $bgImage->user->links->html ?>" target="_blank" title="<?= $bgImage->user->username ?>" class="photo-attribution-component-attribution-link"><?= $bgImage->user->username ?></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php endforeach; ?>
                                            </ul>
                                        </section>
                                        <section>
                                            <header class="background-chooser-header">
                                                <h1 class="background-chooser-heading">
                                                    <span>Цвета</span>
                                                </h1>
                                            </header>
                                            <ul class="background-grid">
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="blue" style="background-color: rgb(131, 140, 145);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="orange" style="background-color: rgb(210, 144, 52);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="green" style="background-color: rgb(81, 152, 57);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="red" style="background-color: rgb(176, 70, 50);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="purple" style="background-color: rgb(137, 96, 158);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                                <li class="background-grid-item">
                                                    <div class="background-grid-trigger is-color" role="button" title="pink" style="background-color: rgb(205, 90, 145);">
                                                        <span class="icon-sm icon-check"></span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    {{ Form::submit("Create", ['class' => 'btn btn-success']) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@push('scripts')
    <script>
	    $('#background-photo div.background-grid-trigger:first').addClass('selected');
    </script>
@endpush
