@extends('layouts.main')

@section('title', $board->name)

@push('styles')
    <style>
        .main-header {
            background: rgba(0,0,0,.32);
            border-bottom: none;
        }
        .main-header * {
            color: white!important;
        }
        .dropdown-menu *{
            color: black!important;
            z-index: 1010;
        }
        .main-panel{
            overflow-y: hidden;
            white-space: nowrap;
            height: 100vh;
            width: 100%;
            @if (!empty($board->bg_image))
background-image: url('{{ $board->bg_image }}');
            background-size: cover;
            background-position: 50%;
            @elseif ($board->bg_color)
background-color: {{ $board->bg_color }};
        @endif
    }
        .sidebar{
            display: none;
        }
        .card-body{
            overflow: auto;
            max-height: 76vh;
        }
        #workflow-panel .card-title{
            display: inline-block;
            color: white;
        }

        .label-card-title{
            width: 215px;
            display: inline-block;
        }

        #workflow-panel .card-title-normal{
            width: 215px;
            background: none;
            color: white;
            border: none;
            margin: 0px;
            font-size: 18px;
            line-height: 1.6;
            padding: 0px;
        }
    </style>
@endpush

@section('content')
    <div class="content" style="padding: 15px 0px 15px 15px;">
        <div class="justify-content-left top-board-panel">
          <div style="display: inline-block; margin: 0 20px;vertical-align: middle;font-size:18px; color: white;">{{ $board->name }}</div>
            <div class="board-header-add-list left-line">
                {{ Form::open(['action' => 'BoardController@addList', 'id' => 'add-list-form']) }}
                    {{ Form::text("name", '', ['class' => 'form-control workflow-title', 'placeholder' => "Ввести заголовок списка", 'autocomplete' => 'off']) }}
                    {{ Form::hidden('boardId', $board->id) }}
                    {{ Form::button("Add list", ['class' => 'btn btn-primary', 'id' => 'btn-add-list']) }}
                {{ Form::close() }}
            </div>
            <div style="display: inline-block;position: relative; margin-right: 10px;">
                <a class="board-header-btn btn-invite" id="board-header-btn-invite" style="position:relative;" href="#" title="Invite To Board">
                    <span class="board-header-btn-text">Invite</span>
                </a>
                @include('board/_invite_form', ['boardId' =>  $board->id])
            </div>
            <div style="display: inline-block;position: relative">
                <a class="board-header-btn btn-users" id="board-header-btn-users" style="position:relative;" href="#" title="Invite To Board">
                    <span class="board-header-btn-text">Users</span>
                </a>
                @include('board/_users', ['board' =>  $board])
            </div>
          <div class="btn btn-primary btn-board-menu">
            Menu
          </div>
        </div>



        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Users Statistics</h4>
                <p class="card-category">
                    Users statistics this month</p>
            </div>
            <div class="card-body">
                <div id="monthlyChart" class="chart chart-pie"></div>
            </div>
        </div>




        <div class="js-add-list list-wrapper mod-add is-idle" id="workflow-panel">
            @foreach ($board->workflow()->orderBy('order', 'asc')->get() as $workflow)
            @include('board/_workflow', ['workflow' => $workflow])
            @endforeach
        </div>
    </div>

{{--    @include('board/_change_card')--}}
    <div id="modalChangeCard" class="window-overlay">

    </div>
@stop


@include ('footer')

@push('scripts')
    <script type="text/javascript" src="{{ asset('assets/js/sortable.js') }}"></script>

    <script>
        Chartist.Pie('#monthlyChart', {
            labels: ['50%', '20%', '30%'],
            series: [51, 20, 30]
        }, {
            plugins: [
                Chartist.plugins.tooltip()
            ]
        });
    </script>
@endpush
