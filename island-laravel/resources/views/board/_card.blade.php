<a href="{{ url('/board/change-card') }}" class="min-card" data-id="{{ $card->id }}">
  <span style="white-space: pre-line;">{{ $card->title }}</span>
  <div class="pencil">
    <img src="{{ asset('assets/img/pencil.png') }}" class="pencil-img">
  </div>
</a>
