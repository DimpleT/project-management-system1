<div id="users" class="pop-over board-member-add-multipl" data-elevation="1" style="left: 0; top: 40px;z-index: 1051;">
    <div class="no-back">
        <div class="pop-over-header js-pop-over-header">
            <span class="pop-over-header-title">Users</span>
            <a href="#" id="btn-close-users" class="pop-over-header-close-btn icon-sm icon-close"></a>
        </div>
        <div>
            <div class="pop-over-content js-pop-over-content u-fancy-scrollbar js-tab-parent" style="max-height: 829px;">
            @foreach ($board->users()->get() as $user)
                <div>{{ $user->name }} <span>x</span></div>
            @endforeach
            </div>
        </div>
    </div>
</div>

@push('scripts')

@endpush
