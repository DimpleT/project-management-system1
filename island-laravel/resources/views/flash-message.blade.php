<div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info', 'primary'] as $msg)
        @if($message = Session::get($msg))
            <div data-notify="container" class="col-xs-11 col-sm-4 alert alert-fixed alert-{{ $msg }} animated" role="alert" data-notify-position="top-right" >
                <button type="button" aria-hidden="true" class="close alert-close" data-notify="dismiss" data-dismiss="alert">×</button>
                <span data-notify="icon" class="la la-bell"></span>
                <span data-notify="title">Notify</span>
                <span data-notify="message">{{ $message }}</span>
            </div>
        @endif
    @endforeach
</div>
