@extends('layouts.main')

@section('title', 'Boards')

@push('styles')
    <style>
        .main-panel{
            width: 100%;
        }
        .sidebar{
            display: none;
        }

    </style>
@endpush

@section('content')
    <div class="content">
        <div class="container">
            <div class="row justify-content-center">
                <h1>Welcome!</h1>
            </div>
        </div>
    </div>
@stop
