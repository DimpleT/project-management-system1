<?php

namespace App\Http\Controllers;

use App\Models\UserBoard;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class UserController{

	public function find(Request $request){
		$text = trim($request->q);

		if (empty($text)) {
			return response()->json([]);
		}

		$users = User::query()->select(['id', 'email', 'name'])
			->where('email', 'like', '%' . $text . '%')
			->orWhere('name','like', '%' . $text . '%')
			->limit(5)
			->get();
		$formatteUser = [];

		foreach ($users as $user) {
			$formatteUser[] = ['id' => $user->id, 'name' => $user->name, 'email' => $user->email];
		}

		return response()->json($formatteUser);
	}

	public function inviteUser(Request $request) {
	    if (!$request->post('user_list') || !$request->post('id_board')) {
            return response()->json(['success' => false, 'error' => '']);
        }

	    $boardId = $request->post('id_board');
	    $userIds = $request->post('user_list');

	    $insert = [];
        foreach ($userIds as $userId) {
            $user = User::query()->select('email')->where('id', $userId)->first();
            $email = $user->email;
            Mail::raw('You invited you to join the board', function($message) use($email) {
                $message->from('bender250222@gmail.com', 'Laravel');
                $message->to($email);
            });
            $insert[] = ['id_user' => $userId, 'id_board' => $boardId];
        }

        if (DB::table('user_board')->insertOrIgnore($insert)) {
            return response()->json(['success' => true, 'error' => '']);
        }

        return response()->json(['success' => false, 'error' => '']);
	}
}
