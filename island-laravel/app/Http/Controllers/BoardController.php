<?php

namespace App\Http\Controllers;

use App\Models\Board;
use App\Models\Workflow;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use JavaScript;
use shweshi\LaravelUnsplashWrapper\UnsplashSearch;

class BoardController extends Controller
{

    public function index($user) {
    	$unsplashPhotos = new UnsplashSearch();
    	$bgImages = json_decode($unsplashPhotos->photos('hd-wallpaper', ['page' => rand(1,100), 'per_page' => 6, 'orientation' => 'landscape']));
        return view('board/index', [
            'singleBoards' => Auth::user()->boards()->where('type', Board::$TYPE_SINGLE)->get(),
            'teamBoards' => Auth::user()->boards()->where('type', Board::$TYPE_TEAM)->get(),
            'bgImages' => $bgImages->results]
        );
    }


    public function create(Request $request) {
    	if (!$request->input('boardName')) {
		    return response()->json(['success' => false, 'error' => 'invalid name']);
	    }

        $folderName = Str::random(6);
        $board = Board::create([
            'name' => $request->input('boardName'),
	        'type' => $request->input('type'),
            'bg_image' => $request->input('bgImage') ?? null,
	        'bg_color' => $request->input('bgColor') ?? null,
            'folder' => $folderName,
        ]);

        if ($board) {
            Storage::makeDirectory('user' . Auth::user()->id . '/' . $folderName , $mode = 0777, true, true);
	        $board->users()->attach(Auth::user());
	        $board->save();
	        return response()->json(['success' => false, 'error' => 'not created', 'url' => URL::to("/board/$folderName/$board->name")]);
        }
        else {
            return response()->json(['success' => false, 'error' => 'not created']);
        }
    }


    public function view($folder, $name) {
    	$board = Auth::user()->boards()->where('folder', $folder)->first();

	    JavaScript::put([
		    'sortWorkflowUrl' => URL::to('/board/sortable-workflow'),
	        'sortCardUrl' => URL::to('/board/sortable-cards'),
		    'renameListUrl' => URL::to('/board/rename-list'),
            'renameCardUrl' => URL::to('/board/rename-card'),
		    'userFindUrl' => URL::to('/user/find')
        ]);


    	return view('/board/view', ['board' => $board]);
    }


    public function addList(Request $request) {
    	if (!$request->input('name') || !$request->input('boardId')) {
		    return response()->json(['success' => false, 'error' => 'not found one of params']);
	    }
        $workflow = Workflow::create([
            'name' => $request->input('name'),
		    'id_board' => $request->input('boardId'),
        ]);

        if ($workflow) {
        	$workflow->update(['order' => $workflow->id]);
        	$view = view('/board/_workflow', [
        		'workflow' => $workflow,
	        ])->render();

        	return response()->json(['html' => $view, 'success' => true, 'error' => '']);
        }
    }


	public function renameList(Request $request) {
		if ($request->input('name') && $request->input('id')) {
			Workflow::where('id', $request->input('id'))->update(['name' => $request->input('name')]);

			return response()->json(['success' => true, 'error' => '']);
		}
	}

    public function renameCard(Request $request) {
        if ($request->input('title') && $request->input('id')) {
            Card::where('id', $request->input('id'))->update(['title' => $request->input('title')]);

            return response()->json(['success' => true, 'error' => '']);
        }
    }

    public function addCard(Request $request) {
    	if (!$request->input('title') || !$request->input('listId')) {
    		return response()->json(['success' => false, 'error' => 'invalid name']);
    	}
    	$card = Card::create([
    		'title' => $request->input('title'),
		    'id_workflow' => $request->input('listId'),
	    ]);

    	if ($card) {
    		$card->update(['order' => $card->id]);
    		return response()->json(['html' => view('/board/_card', ['card' => $card])->render(), 'success' => true, 'error' => '']);
    	}
    	else {
    		return response()->json(['success' => false, 'error' => 'not created']);
    	}

    }


    public function delete($id) {
        if ($id) {
            if ($board = Board::find($id)) {
                File::deleteDirectory('user' . $board->id_user . '/' . $board->folder);

                //$board->delete();
            }
            return redirect()->back()->with('success','Item created successfully!');
        }
    }


    public function deleteList($id) {
    	if ($id && $workflow = Workflow::find($id)) {
    		$workflow->delete();
		    Session::flash('warning', 'List deleted successfully!');
    	}
    	else {
		    Session::flash('warning', 'List not deleted!');
	    }

	    return redirect()->back();
    }


    public function updateWorkflowOrder(Request $request) {
    	if ($request->order) {
    		foreach ($request->order as $val) {
    			$forMove = Workflow::find($val['id']);
    			$forMove->update(['order' => $val['position']]);
    		}
    	}

    	return response('Update Successfully.', 200);
    }


    public function updateCardOrder(Request $request) {
    	if ($request->order) {
    		foreach ($request->order as $val) {
    			$forMove = Card::find($val['id']);
    			$forMove->update(['order' => $val['position'], 'id_workflow' => $request->input('workflowId')]);
    		}
    	}

    	return response('Update Successfully.', 200);
    }


    public function changeCard(Request $request){
        $id = $request->get('id');

        $view = view('/board/_change_card', ['card' => Card::find($id)])->render();

        return response()->json(['success' => true, 'html' => $view]);
    }
}
