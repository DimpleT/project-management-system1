<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
  protected $table = 'card';
  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = ['title', 'id_workflow', 'order'];
}
