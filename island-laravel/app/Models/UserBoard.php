<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserBoard extends Pivot
{
	protected $table = 'user_board';

    protected $fillable = ['id_user', 'id_board'];
}
