<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Workflow extends Model
{
  protected $table = 'workflow';
  /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
   protected $fillable = ['name', 'id_board', 'order'];

   /**
   * Get the comments for the blog post.
   */
  public function cards()
  {
      return $this->hasMany('App\Models\Card', 'id_workflow');
  }
}
