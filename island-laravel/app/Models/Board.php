<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{

	public static $TYPE_SINGLE = 1;
	public static $TYPE_TEAM = 2;

    protected $table = 'board';
    /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = ['name', 'id_user', 'folder', 'type', 'bg_image', 'bg_color'];

     /**
     * Get the comments for the blog post.
     */
    public function workflow()
    {
        return $this->hasMany('App\Models\Workflow', 'id_board');
    }

	public function users() {
		return $this->belongsToMany(User::class, 'user_board', 'id_board', 'id_user');
	}
}
